# Figma Ebooks UI


## Team Member
* นายวรปกร      จารุศิริพจน์
* นางสาวนวพร	บุญก่อน
* นายณัฐวุฒิ      แก้วมหา
* นายธีรณัฏฐ์      นุชเจริญ 

## Directory Structure
project/

│

├── Worapakorn/ # โฟลเดอร์ Worapakorn  ให้แนบรูป Figma ที่ทำเสร็จแล้วมา มี -edit-book.png -after-login-page.png -menu-user-book.png -confirm-delete-section.png

├── Nattawut/ # โฟลเดอร์ Nattawut ให้แนบรูป Figma ที่ทำเสร็จแล้วมา มี -show-list-book-user-edit.png -show-detail-book-page-for-edit.png -add-section-book.png -login-error-password-fail.png

├── Nawaporn/ # โฟลเดอร์ Nawaporn ให้แนบรูป Figma ที่ทำเสร็จแล้วมา มี -login.png -chang-profile-image.png -user-menu.png

├── Teeranat/ # โฟลเดอร์ Teeranat ให้แนบรูป Figma ที่ทำเสร็จแล้วมา มี -add-book-page.png -edit-section-book.png -edit-user-profile.png -main-page.png

└── README.md


## ตำแหน่งงาน

ลิงค์ Figma Ebooks UI
https://www.figma.com/file/TXo9yUKMvWRMQtgXMtFPg7/Figma-Ebooks-UI?type=design&node-id=0%3A1&mode=design&t=pYzZBNLQXH9lNSEe-1
